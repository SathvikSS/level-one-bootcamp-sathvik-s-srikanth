//Write a program to add two user input numbers using one function.
#include <stdio.h>
float Sum(float a,float b)
{
    float s=a+b;
    return s;
}
int main()
{
    float a,b,sum;
    printf("Enter two numbers\n");
    scanf("%f %f",&a,&b);
    sum= Sum(a,b);
    printf("%.2f+%.2f=%.2f\n",a,b,sum);
    return 0;
}


