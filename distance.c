//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
float input()
{
    float a; 
    printf("Enter a co ordinate\n");
    scanf("%f",&a);
    return a;
}
float find_dist(float a, float b, float x, float y)
{
    float d;
    d= sqrt(pow(x-a,2)+pow(y-b,2));
    return d;
}

void output(float dist)
{
    printf("Diatance is %f\n",dist);
}

int main()
{
    float a,b,x,y,Dist;
    a=input();
    b=input();
    x=input();
    y=input();
    Dist=find_dist(a,b,x,y);
    output(Dist);
    return 0;
}